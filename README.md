# Ansible Role:

Install and set up fail2ban in RHEL/Fedora, Debian/Ubuntu.

## Requirements

None

## Role Variables

	fail2ban_loglevel: INFO
	fail2ban_ignoreips: []
	fail2ban_bantime: 600
	fail2ban_maxretry: 3
	fail2ban_findtime: 600
	fail2ban_banaction: firewallcmd-ipset
	fail2ban_mta: sendmail
	fail2ban_sendername: 'Fail2ban'
	fail2ban_destemail: root@localhost
	fail2ban_purge_days: 15

- fail2ban_loglevel: [default`INFO`: Sets the loglevel output (e.g. `1 = ERROR`, `2 = WARN`, `3 = INFO`, `4 = DEBUG`)
- fail2ban_ignoreips: Which IP address/CIDR mask/DNS host should be ignored from fail2ban's actions
- fail2ban_bantime: [default: `600`]: Sets the bantime
- fail2ban_maxretry: [default: `3`]: Maximum number of retries before the host is put into jail
- fail2ban_findtime: [default: `600`]: A host is banned if it has generated `fail2ban_maxretry` during the last `fail2ban_findtime`
- fail2ban_banaction: [default: `iptables-multiport`]: Sets the global/default banaction
- fail2ban_sendername: [default: `Fail2ban`]: The 'from' display name for emails sent by mta actions (may not be an email address).
___ fail2ban_sender to set the 'from' email address. ___
- fail2ban_sender: [optional]: The 'from' address for emails sent by mta actions. [example: fail2ban@local.local]
- fail2ban_destemail: [optional]: The email address that should receive ban messages. [example: root@local.local]

## Dependencies

None

## Example Playbook

    - hosts: webservers
      vars_files:
        - vars/main.yml
      roles:
        - { role: dsg-role-fail2ban }

## License

MIT

## Author Information

This role was created in 2021 by Didier MINOTTE
